extends MultiplayerSpawner


var playerScene: PackedScene = preload("res://entity/player/Player.tscn")


func _spawn_custom(data) -> Object:
	print("PlayerSpawner._spawn_custom")
	var id: int = data[0]
	var playerName: String = data[1]
	var pos: Vector2 = data[2]
	
	var playerNode: Player = playerScene.instantiate()
	playerNode.name = str(id)
	playerNode.setId(id)
	playerNode.setName(playerName)
	playerNode.ready.connect(playerNode.setPosition.bind(pos), CONNECT_ONESHOT)
	
	return playerNode
