extends Node2D


class ActivePlayer:
	var id: int
	var name: String
	var node: Node2D
	
	func _init(i, na, no):
		id = i
		name = na
		node = no


@onready var EntityLayer: Node2D = $EntityLayer
@onready var PlayerLayer: Node2D = $PlayerLayer
@onready var UniqueSpawns: Node2D = $UniqueSpawns
@onready var PlayerSpawner: MultiplayerSpawner = $PlayerSpawner
@onready var EntitySpawner: MultiplayerSpawner = $EntitySpawner

var activePlayers := {}


func getUseableSpawnPoint() -> Vector2:
	var spawnPoints: Array[Node2D] = get_tree().get_nodes_in_group("SpawnPoint")
	randomize()
	spawnPoints.shuffle()
	
	for sp in spawnPoints:
		if sp.canUse():
			return sp.global_position
	
	return Vector2.ZERO


func _handlePlayerToggle(id: int, didJoin: bool) -> void:
	print("World toggle player " + str(didJoin) + ", id = " + str(id))
	
	if didJoin:
		var playerNode: Player = _spawnPlayer(id)
#		PlayerLayer.add_child(playerNode, true)
	
	else:
		var activePlayer: ActivePlayer = activePlayers.get(id, null)
		
		if is_instance_valid(activePlayer.node):
			PlayerLayer.remove_child(activePlayer.node)
			activePlayers.erase(id)

func _spawnPlayer(id: int) -> Player:
	var playerName: String = "Player " + str(id)
	var spawnPoint: Vector2 = getUseableSpawnPoint()
	var playerNode = PlayerSpawner.spawn([id, playerName, spawnPoint])
	
	var ap := ActivePlayer.new(id, playerName, playerNode)
	activePlayers[id] = ap
	
	return playerNode

@rpc(any_peer)
func _handlePlayerSpawned(playerId: int) -> void:
	if multiplayer.is_server():
		var ap: ActivePlayer = activePlayers.get(playerId)
		
		ap.node.setId.rpc(ap.id)
		ap.node.setName.rpc(ap.name)
		ap.node.setPosition.rpc(ap.node.global_position)
		ap.node.networkReady.rpc()

func _handleClientPlayerSpawned(playerNode: Player) -> void:
	_handlePlayerSpawned.rpc_id(1, str(playerNode.name).to_int())

func _handleDangerDepleted() -> void:
	var swarm = SwarmOracle.createSwarm(SwarmOracle.EnemyType.DrillDrone, int(randf() * 7 + 1))
	var player: Node2D = PlayerLayer.get_children()[0]
	
	if swarm == null:
		SwarmOracle.start()
		return
	
	for swarmEnemy in swarm.enemies:
		swarmEnemy.node.global_position = player.global_position + randf() * Vector2(512, 128)
		EntityLayer.add_child(swarmEnemy.node)


func _ready() -> void:
	print("World ready, id = " + str(multiplayer.get_unique_id()))
	
	PhysicsSpaceOracle.init($LocalPhysicsLayer)
	
	$AmbianceStreamPlayer/AnimationPlayer.play("Loop")
	
	if multiplayer.is_server():
		NetworkOracle.playerToggle.connect(_handlePlayerToggle)
		for p in NetworkOracle.getPlayers():
			_handlePlayerToggle(p.id, true)
		
		for entity in UniqueSpawns.get_children():
			print("World ready, spawning entity " + str(entity.name))
			EntitySpawner.spawn([entity.name, entity.global_position])
	
	else:
		PlayerLayer.child_entered_tree.connect(_handleClientPlayerSpawned)
	
	SwarmOracle.dangerDepleted.connect(_handleDangerDepleted)
#	SwarmOracle.start()
