extends MultiplayerSpawner


var scenes := {
	"Mothership2": preload("res://entity/vehicles/mothership/Mothership2.tscn"),
	"Doomball": preload("res://entity/enemies/doomball/Doomball.tscn"),
}


func setPosition(node: Node2D, pos: Vector2) -> void:
	node.global_position = Vector2.ZERO
	node.setPosition(pos)


func _spawn_custom(data) -> Object:
	var key: String = data[0]
	var node: Node2D = scenes[key].instantiate()
	var pos: Vector2 = data[1]
	
	node.global_position = Vector2(42069, -42069)
	node.ready.connect(setPosition.bind(node, pos), CONNECT_ONESHOT)
	
	return node
