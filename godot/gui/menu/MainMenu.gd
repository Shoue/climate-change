extends Node2D


@onready var HostField: TextEdit = get_node("%HostTextEdit")
@onready var PortField: TextEdit = get_node("%PortTextEdit")

func _on_join_button_pressed() -> void:
	var host: String = HostField.text if HostField.text != "" else "localhost"
	var port: int = PortField.text.to_int() if PortField.text != "" else 42069
	NetworkOracle.startClient(host, port)
	get_tree().change_scene("res://world/World.tscn")

func _on_host_button_pressed() -> void:
	NetworkOracle.startServer()
	get_tree().change_scene("res://world/World.tscn")
