extends Node2D

signal playerToggle (id, didJoin)


class RegisteredPlayer:
	var id: int
	var name: String
	
	func _init(i: int, n: String):
		id = i
		name = n
	
	func getId() -> int:
		return id
	
	func getName() -> String:
		return name


const MAX_PEERS: int = 64
const DEFAULT_PORT: int = 42069

# playerDict : { [K: Int]: RegisteredPlayer }
@export var playerDict: Dictionary = {}

var peer := ENetMultiplayerPeer.new()
var waitList: Array = []


func startClient(server: String, port: int) -> void:
	peer.create_client(server, port)
	multiplayer.set_multiplayer_peer(peer)
	multiplayer.connected_to_server.connect(_handleServerConnected)
	multiplayer.connection_failed.connect(_handleConnectionFailed)
	multiplayer.server_disconnected.connect(_handleServerDisconnected)

func startServer() -> void:
	peer.create_server(DEFAULT_PORT, MAX_PEERS)
	multiplayer.set_multiplayer_peer(peer)
	multiplayer.peer_connected.connect(_handlePeerConnected)
	multiplayer.peer_disconnected.connect(_handlePeerDisconnected)
	
	playerDict[1] = RegisteredPlayer.new(1, "Player 1")
	playerToggle.emit(1, true)

func registerName(pn: String) -> void:
	var id: int = multiplayer.get_unique_id()
	var playerPair = playerDict.get(id)
	playerPair.name = pn

func getRegisteredPlayer(id: int) -> RegisteredPlayer:
	return playerDict.get(id, null)

func isControllable(id: int) -> bool:
	return multiplayer.multiplayer_peer == null or multiplayer.get_unique_id() == id

func getPlayers() -> Array[RegisteredPlayer]:
	return playerDict.values()


func _handlePeer(id: int, didJoin: bool) -> void:
	if didJoin and not waitList.has(id):
		waitList.append(id)
		playerDict[id] = RegisteredPlayer.new(id, "Player " + str(id))
		playerToggle.emit(id, true)
	
	elif not didJoin:
		if waitList.has(id):
			waitList.erase(id)
		
		if playerDict.has(id):
			playerDict.erase(id)
			playerToggle.emit(id, false)

func _handlePeerConnected(id: int) -> void:
	print("NOracle peer connected = " + str(id))
	_handlePeer(id, true)

func _handlePeerDisconnected(id: int) -> void:
	print("NOracle peer disconnected = " + str(id))
	_handlePeer(id, false)

func _handleServerConnected() -> void:
	print("NOracle server connected")

func _handleConnectionFailed() -> void:
	print("NOracle connection failed")

func _handleServerDisconnected() -> void:
	print("NOracle server disconnected")
