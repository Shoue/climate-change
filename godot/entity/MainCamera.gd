extends Camera2D


var body: Node2D = null


func _physics_process(delta: float) -> void:
	if body != null: global_position = body.global_position
