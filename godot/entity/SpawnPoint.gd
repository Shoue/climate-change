extends Node2D


@onready var SpawnArea: Area2D = $Area2D


func canUse() -> bool:
	var bodies: Array = SpawnArea.get_overlapping_bodies()
	return bodies.size() == 0
