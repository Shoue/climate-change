extends Node2D

class_name Player


const inputMappings := {
	"jump": {
		"pressed": InputMappings.Action.Jump,
		"released": InputMappings.Action.Jump,
	},
	"walk_left": {
		"pressed": InputMappings.Action.WalkLeft,
		"released": InputMappings.Action.WalkLeft,
	},
	"walk_right": {
		"pressed": InputMappings.Action.WalkRight,
		"released": InputMappings.Action.WalkRight,
	},
	"fire_gun": {
		"pressed": InputMappings.Action.Shoot,
		"released": InputMappings.Action.Shoot,
	},
	"inventory_1": {
		"pressed": InputMappings.Action.Inventory_1
	},
	"inventory_2": {
		"pressed": InputMappings.Action.Inventory_2
	},
	"aim_up": {
		"pressed": InputMappings.Action.AimUp,
		"released": InputMappings.Action.AimUp,
	},
	"aim_down": {
		"pressed": InputMappings.Action.AimDown,
		"released": InputMappings.Action.AimDown,
	},
	"use": {
		"pressed": InputMappings.Action.Use,
		"released": InputMappings.Action.Use,
	}
}

@onready var jumpTween: Tween
@onready var Lib = $Lib
@onready var Physics: RigidDynamicBody2D = %Physics
var Root = self
@onready var Visuals = %Visuals

var playerId: int = -1
var playerName: String = "Unknown"
var movement := Vector2.ZERO
var stateSpeed := Vector2.ZERO
var addedSpeed := Vector2.ZERO


@rpc(authority)
func setId(id: int) -> void:
	playerId = id

@rpc(authority)
func setName(pn: String) -> void:
	playerName = pn

@rpc(authority)
func setPosition(p: Vector2) -> void:
	var P = Visuals.getTrackedNode()
	var diff: Vector2 = p - P.global_position
	if diff.length() > 128.0:
		P.global_position = p
	if diff.length() > 4.0:
		addedSpeed = diff.normalized()
	else:
		addedSpeed = Vector2.ZERO

func _handleDeath() -> void:
	Visuals.setTrackedNode(Physics)
	Visuals.setOffset(Physics, Vector2.ZERO)
	Physics.setTrackedNode(null)
	Physics.global_position = Vector2.ZERO
	Physics.linear_velocity = Vector2.ZERO
	
	Lib.HealthManager.setHealth(100.0)

func _handleInputMapping(type: String, isPressed: bool) -> void:
	if not NetworkOracle.isControllable(playerId): return
	_handleMappingEvent.rpc(type, isPressed)

@rpc(any_peer, call_local)
func _handleMappingEvent(type: String, isPressed: bool) -> void:
	if type == InputMappings.Action.WalkLeft:
		var n = 1 if isPressed else -1
		movement.x = max(-1, movement.x - n)
	elif type == InputMappings.Action.WalkRight:
		var n = 1 if isPressed else -1
		movement.x = min(1, movement.x + n)
	
	if movement.x != 0.0:
		Visuals.scale.x = -sign(movement.x)
	
	if type == InputMappings.Action.Jump and isPressed:
		movement.y = -1.0
		jumpTween = get_tree().create_tween()
		jumpTween.tween_property(self, "movement:y", 0.0, 0.6)
		return
	elif type == InputMappings.Action.Jump:
		jumpTween.kill()
		movement.y = 0.0
		return
	
	if type == InputMappings.Action.AimUp or type == InputMappings.Action.AimDown:
		var vertical := Input.get_axis("aim_down", "aim_up")
		Lib.WeaponHolder.setRotation(deg2rad(90 * sign(vertical)))
	
	if type == InputMappings.Action.Shoot and isPressed:
		var vertical := Input.get_axis("aim_down", "aim_up")
		
		if vertical != 0.0:
			Lib.WeaponHolder.shoot(Vector2.UP * vertical)
		else:
			Lib.WeaponHolder.shoot(Vector2.RIGHT * -Visuals.scale)
	elif type == InputMappings.Action.Shoot:
		Lib.WeaponHolder.deactivate()
	
	if type == InputMappings.Action.Inventory_1: Lib.WeaponHolder.activateSlot(1)
	elif type == InputMappings.Action.Inventory_2: Lib.WeaponHolder.activateSlot(2)


func _input(event: InputEvent) -> void:
	if Lib.ControlLock.isLocked(): return
	if not NetworkOracle.isControllable(playerId): return
	
	if event is InputEventKey and event.as_text() == "Escape":
		_handleDeath()

func _physics_process(delta: float) -> void:
	var P = Visuals.getTrackedNode()
	if not (P is RigidDynamicBody2D): return

	var speed := Vector2(4, 10)
	
	var targetSpeed := movement * speed
	
	stateSpeed = stateSpeed * 0.7 + targetSpeed * 0.3
	P.move_and_collide(stateSpeed + addedSpeed)
	
	if P.global_position.length() > 1024 * 8:
		P.global_position = Vector2(0, -512)
		P.linear_velocity = Vector2.ZERO
	
	if is_multiplayer_authority():
		setPosition.rpc(P.global_position)

func _ready() -> void:
	Physics.init(Visuals)
	Visuals.init(Physics)
	
	Lib.InputMappings.init(inputMappings)
	Lib.InputMappings.action.connect(_handleInputMapping)
	
	Lib.HealthManager.healthDepleted.connect(_handleDeath)
	
	if is_multiplayer_authority():
		networkReady()

@rpc(authority)
func networkReady() -> void:
	print(playerName + " networkReady")
	
	if NetworkOracle.isControllable(playerId):
		var cam: Camera2D = get_node("/root/World/MainCamera")
		cam.body = Visuals
		$Visuals/AudioListener2D.current = true
	
	Lib.NameLabel.text = playerName
