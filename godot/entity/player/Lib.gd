extends Node2D

class_name PlayerLib


var ControlLock = preload("res://entity/control/ControlLock.gd").new()
@onready var HealthManager: HealthManager = %HealthManager
@onready var InputMappings: InputMappings = %InputMappings
@onready var NameLabel: Label = %PlayerName/Label
@onready var WeaponHolder = %WeaponHolder


func _ready() -> void:
	print("PlayerLib _ready()")
