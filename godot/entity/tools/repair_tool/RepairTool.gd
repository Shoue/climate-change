extends Weapon

class_name RepairTool


@onready var A: Area2D = $Area2D
@onready var Ray: RayCast2D = $RayCast2D
@onready var BeamSprite: Sprite2D = $Beam/Sprite2D
@onready var AudioPlayer: AnimationPlayer = $AudioStreamPlayer2D/AnimationPlayer

var active := false
var direction := Vector2.ZERO
var activeBodies: Array[Node2D] = []


func shoot(dir: Vector2) -> void:
	active = true
	direction = dir.normalized()
	AudioPlayer.play("Loop")

func deactivate() -> void:
	active = false
	AudioPlayer.stop()


func _handleBody(body: Node2D, didEnter: bool) -> void:
	body = body.Root if body.get("Root") != null else body
	if body.get("HealthManager") == null: return
	
	if didEnter:
		activeBodies.append(body)
		set_physics_process(true)
	else:
		activeBodies.erase(body)
		set_physics_process(activeBodies.size() > 0)

func _makeBeam() -> void:
	print(direction)
	Ray.target_position = Ray.global_position + direction * 1024.0
	Ray.force_raycast_update()
	var p: Vector2 = Ray.get_collision_point()
	A.global_position = p
	
	var distance := Ray.global_position.distance_to(p)
	var size := Vector2(distance, 8)
	BeamSprite.position = Vector2(distance * 0.5, 0)
	BeamSprite.region_rect = Rect2(Vector2.ZERO, size)


func _physics_process(delta: float) -> void:
	if not active: return
	_makeBeam()
	
	for body in activeBodies:
		body.HealthManager.heal(delta * 10)

func _ready() -> void:
	A.area_entered.connect(_handleBody.bind(true))
	A.area_exited.connect(_handleBody.bind(false))
	A.body_entered.connect(_handleBody.bind(true))
	A.body_exited.connect(_handleBody.bind(false))
