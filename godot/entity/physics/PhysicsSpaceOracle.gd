extends Node2D


@onready var LocalPhysicsSpace = $LocalPhysicsSpace

var stage: Node2D


func makePhysicsSpace() -> LocalPhysicsSpace:
	var space: LocalPhysicsSpace = LocalPhysicsSpace.duplicate()
	stage.add_child(space)
	return space


func init(s: Node2D) -> void:
	stage = s
