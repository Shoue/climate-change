extends Node2D

class_name VisualPart


var trackedNode: Node2D
var offset := Vector2.ZERO


func setTrackedNode(node: Node2D) -> void:
	print("Visuals.setTrackedNode = " + str(node.get_path()))
	trackedNode = node

func getTrackedNode() -> Node2D:
	return trackedNode

func setOffset(node: Node2D, o: Vector2) -> void:
	if node == trackedNode:
		offset = o


func _process(delta: float) -> void:
	if is_instance_valid(trackedNode):
		global_position = trackedNode.global_position + offset

func init(node: Node2D) -> VisualPart:
	trackedNode = node
	
	return self
