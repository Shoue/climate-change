extends RigidDynamicBody2D

class_name PhysicsPart


var Physics = self
var Root: Node2D = null
var Visuals = null
var integrateForces
var trackedNode: Node2D = null


func setTrackedNode(node: Node2D) -> void:
	print(str(get_path()) + " physics tracking node = " + str(node))
	if (Visuals == null or Visuals.getTrackedNode() != self) and is_instance_valid(node):
		trackedNode = node
	elif node == null:
		trackedNode = null

func getTrackedNode() -> Node2D:
	return trackedNode

func clone(excluded: Array) -> void:
	var dupNode: PhysicsPart = duplicate()
	
	print("PP.clone, excluded = " + str(excluded))
	for e in excluded:
		var exNode = dupNode.get_node(e)
		print(exNode.get_children())
		print(exNode)
		dupNode.remove_child(exNode)
	
	return dupNode


func _integrate_forces(state: PhysicsDirectBodyState2D) -> void:
	if trackedNode != null:
		state.transform.origin = trackedNode.global_position
	
	elif integrateForces != null:
		self.integrateForces.call(state)

func init(v: VisualPart) -> PhysicsPart:
	Visuals = v
	if Root == null: Root = $".."
	
	return self
