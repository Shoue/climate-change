extends Node2D

class_name LocalSpace


var ownerNode: Node2D
var area: Area2D
var space: LocalPhysicsSpace
var stage: Node2D
var originalPhysics: PhysicsPart
var selfLocalPhysics: RigidDynamicBody2D
var globalDict := {}
var localDict := {}


func getLocalPhysics() -> RigidDynamicBody2D:
	return selfLocalPhysics


func _handleBody(body: Node2D, didEnter: bool) -> void:
	_handleClientBody.rpc(body, didEnter)

@rpc(authority, call_local)
func _handleClientBody(body: Node2D, didEnter: bool) -> void:
	print("LS didEnter = " + str(didEnter) + ", body = " + str(body.get_path()))
	var Physics = body.get("Physics")
	if not (Physics is RigidDynamicBody2D): return
	
	var physPath: NodePath = Physics.get_path()
	
	if didEnter and not globalDict.has(physPath):
		var localPhysics: RigidDynamicBody2D = Physics.clone(["FlightControls"])
		localPhysics.Root = Physics.Root
		localPhysics.global_position = originalPhysics.to_local(Physics.global_position)
		stage.add_child(localPhysics)
		body.Visuals.setTrackedNode(localPhysics)
		body.Physics.setTrackedNode(body.Visuals)
		
		print("LocalSpace added local physics " + str(localPhysics.get_path()) + " to " + str(localPhysics.position))
		
		globalDict[physPath] = localPhysics
		localDict[localPhysics.get_path()] = Physics
		
	elif not didEnter and localDict.has(physPath):
		var originalBody = localDict[physPath].Root
		var localPhysics = Physics
		
		originalBody.Physics.global_position = originalPhysics.to_global(localPhysics.position)
		originalBody.Physics.setTrackedNode(null)
		originalBody.Visuals.setTrackedNode(originalBody.Physics)
		originalBody.Visuals.setOffset(originalBody.Physics, Vector2.ZERO)
		
		call_deferred("_handleErase", originalBody.Physics.get_path())
		
		print("LocalSpace removed local physics " + str(localPhysics.get_path()) + " from " + str(localPhysics.global_position))
		stage.remove_child(localPhysics)

func _handleErase(physicsPath: NodePath) -> void:
	var localPhysics = globalDict.get(physicsPath)
	globalDict.erase(physicsPath)
	localDict.erase(localPhysics.get_path())


func _physics_process(delta: float) -> void:
	var offset: Vector2 = originalPhysics.global_position
	for k in localDict:
		var NodeVisuals: VisualPart = localDict[k].Visuals
		var localPhysics = globalDict.get(localDict[k].get_path())
		NodeVisuals.setOffset(localPhysics, offset)


func init(on: Node2D, p: RigidDynamicBody2D, a: Area2D) -> LocalSpace:
	ownerNode = on
	area = a
	originalPhysics = p
	
	stage = Node2D.new()
	stage.name = "Stage"
	
	space = PhysicsSpaceOracle.makePhysicsSpace()
	
	var View = space.getView()
	View.add_child(stage, true)
	
	var localArea := area.duplicate()
	View.add_child(localArea)
	
	selfLocalPhysics = p.duplicate()
	selfLocalPhysics.global_position = Vector2.ZERO
	selfLocalPhysics.freeze = true
	View.add_child(selfLocalPhysics, true)
	
	area.body_entered.connect(_handleBody.bind(true))
	localArea.body_exited.connect(_handleBody.bind(false))
	
	return self
