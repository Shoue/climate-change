extends Node2D

class_name InputMappings

# TODO support _registering_ actions
# - we need each entity to have its own set of actions instead of everything all at once for everyone

signal action (inputAction, isPressed)


const Action := {
	"Shoot": "Shoot",
	"WalkLeft": "WalkLeft",
	"WalkRight": "WalkRight",
	"AimUp": "AimUp",
	"AimDown": "AimDown",
	"Jump": "Jump",
	"Inventory_1": "Inventory_1",
	"Inventory_2": "Inventory_2",
	"Use": "Use",
}

var inputMappings := {}


func _input(event: InputEvent) -> void:
	for action in inputMappings:
		var mapping: Dictionary = inputMappings[action]
		
		if "pressed" in mapping and event.is_action_pressed(action):
			self.action.emit(mapping.pressed, true)
		
		elif "released" in mapping and event.is_action_released(action):
			self.action.emit(mapping.released, false)

func init(ms: Dictionary) -> InputMappings:
	inputMappings = ms
	return self
