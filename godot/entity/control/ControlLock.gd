extends Node

class_name ControlLock


var lockers: Array[Node2D] = []


func lock(node: Node2D) -> void:
	if not lockers.has(node): lockers.append(node)

func unlock(node: Node2D) -> void:
	lockers.erase(node)

func isLocked() -> bool:
	return lockers.size() > 0
