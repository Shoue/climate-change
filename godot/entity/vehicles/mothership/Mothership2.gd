extends Node2D

class_name Mothership


const speed: Vector2 = Vector2(100, 100)

@export var networkVelocity := Vector2.ZERO
@export var networkPosition := Vector2.ZERO

@onready var Physics: PhysicsPart = $Physics
@onready var Visuals: VisualPart = $Visuals
@onready var LocalSpace = $Visuals/LocalSpace

var Root = self


func integrateForces(state: PhysicsDirectBodyState2D) -> void:
	if is_multiplayer_authority():
		var delta: float = state.step
		var time = Time.get_unix_time_from_system()
		var moveSpeed = Vector2(sin(time * PI), cos(time * 0.1 * PI)) * speed
		
		var v: Vector2 = state.linear_velocity
		networkVelocity = Vector2((v.x + moveSpeed.x) / 2.0, (v.y + moveSpeed.y) / 2.0)
		state.linear_velocity = networkVelocity
		networkPosition = global_position
	
	else:
		if global_position.distance_to(networkPosition) > 8.0:
			global_position = networkPosition
		state.linear_velocity = networkVelocity

func _ready() -> void:
	Physics.init(Visuals)
	Visuals.init(Physics)
	LocalSpace.init(self, Physics, $Visuals/LocalArea)
	
	Physics.integrateForces = integrateForces
