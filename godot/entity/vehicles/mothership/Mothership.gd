extends Node2D

class_name Mothership2


const speed: Vector2 = Vector2(100, 100)

@export var networkVelocity := Vector2.ZERO
@export var networkPosition := Vector2.ZERO

@onready var Physics: PhysicsPart = $Physics
@onready var Visuals: VisualPart = $Visuals
@onready var LocalSpace = $Visuals/LocalSpace
@onready var DestructiveMap: Destructive = $Physics/Destructive

var Root = self


@rpc(authority)
func setPosition(p: Vector2) -> void:
	Physics.global_position = p


func integrateForces(state: PhysicsDirectBodyState2D) -> void:
	if is_multiplayer_authority():
		var t: float = Time.get_ticks_msec() * 0.001
		state.linear_velocity = Vector2(cos(t * 0.5), sin(t * 1.0)) * 64.0
		
		networkVelocity = state.linear_velocity
		networkPosition = state.transform.origin
	
	else:
		if state.transform.origin.distance_to(networkPosition) > 8.0:
			state.transform.origin = networkPosition
		state.linear_velocity = networkVelocity

func _ready() -> void:
	Physics.init(Visuals)
	Visuals.init(Physics)
	LocalSpace.init(self, Physics, $Visuals/LocalArea)
	var localPhysics = LocalSpace.getLocalPhysics()
	var localDestructive = localPhysics.get_node("Destructive")
	
	DestructiveMap.registerLocal(localDestructive.getTileMap())
	
	$Visuals/AmbianceStreamPlayer/AnimationPlayer.play("Loop")
	
	Physics.integrateForces = integrateForces
