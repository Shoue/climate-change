extends PathFollow2D

class_name Door

@export var areaPath: NodePath

@onready var Body: AnimatableBody2D = $AnimatableBody2D
@onready var Area: Area2D = get_node(areaPath)

const speed: float = 1.0

var isEnabled := false
var doorTween: Tween


func _toggleDoor(_b: Node2D, didEnter: bool) -> void:
	print("Door._toggleDoor, " + str(get_path()) + " = " + str(didEnter))
	if didEnter:
		doorTween = get_tree().create_tween()
		doorTween.tween_property(self, "unit_offset", 1.0, 0.2)

func _startTimer(_b: Node2D) -> void:
	if unit_offset > 0.9:
		$Timer.start()
	else:
		doorTween = get_tree().create_tween()
		doorTween.tween_property(self, "unit_offset", 0.0, 0.3)


func _ready() -> void:
	Area.connect("body_entered", _toggleDoor, [true])
	Area.connect("body_exited", _startTimer)
	$Timer.connect("timeout", _toggleDoor, [null, false])
