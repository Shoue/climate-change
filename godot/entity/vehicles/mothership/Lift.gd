extends PathFollow2D

class_name Lift

@onready var Body: AnimatableBody2D = $AnimatableBody2D
@onready var Area: Area2D = $AnimatableBody2D/Area2D

const speed: float = 1.0

var isEnabled := false


func _toggleLift(body: Node2D, didEnter: bool) -> void:
	print("Lift._toggleLift, " + str(get_path()) + " = " + str(didEnter))
	isEnabled = didEnter

func _startTimer(_b: Node2D) -> void:
	if unit_offset > 0.4:
		$Timer.start()
	else:
		isEnabled = false


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	Area.connect("body_entered", _toggleLift, [true])
	Area.connect("body_exited", _startTimer)
	$Timer.connect("timeout", _toggleLift, [null, false])

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta: float) -> void:
	var curve: float = cos(unit_offset * PI - 0.5 * PI) * 0.5 + 0.5
	
	if not $Timer.is_stopped():
		pass
	elif isEnabled:
		unit_offset += delta * speed * curve
	else:
		unit_offset -= delta * speed * curve
