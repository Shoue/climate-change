extends RigidDynamicBody2D

class_name Dropship


var addedSpeed := Vector2.ZERO


func _ready() -> void:
	pass # Replace with function body.


func _physics_process(delta: float) -> void:
	linear_velocity = addedSpeed
