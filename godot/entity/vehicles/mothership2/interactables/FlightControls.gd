extends Node2D


const SPEED := Vector2(64, 128)

@export var ShipPhysicsPath: NodePath
@onready var ShipPhysics: PhysicsPart = get_node(ShipPhysicsPath)

var user: Node2D
var bodies: Array[Node2D] = []


func _handlePlayer(body: Node2D, didEnter: bool) -> void:
	print("FlightControls player " + str(body) + " entered? " + str(didEnter))
	if didEnter: bodies.append(body)
	else: bodies.erase(body)


func integrateForces(state: PhysicsDirectBodyState2D) -> void:
	var flyAxisH: float = Input.get_axis("fly_left", "fly_right")
	var flyAxisV: float = Input.get_axis("fly_up", "fly_down")
	var movement = Vector2(flyAxisH, flyAxisV)
	
	var targetVelocity: Vector2 = movement * SPEED
	state.linear_velocity = state.linear_velocity * 0.7 + targetVelocity * 0.3


func _input(event: InputEvent) -> void:
	if bodies.size() == 0: return
	
	if event.is_action_pressed("use") and user != null:
		print("FlightControls use = false")
		user.Root.ControlLock.unlock(self)
		user.Physics.setTrackedNode(null)
		var cam: Camera2D = get_node("/root/World/MainCamera")
		cam.zoom = Vector2.ONE * 2
		user = null
		ShipPhysics.integrateForces = ShipPhysics.Root.integrateForces
	
	elif event.is_action_pressed("use") and user == null:
		print("FlightControls use = true")
		user = bodies[0]
		
		user.Root.Lib.ControlLock.lock(self)
		user.Physics.setTrackedNode(self)
		var cam: Camera2D = get_node("/root/World/MainCamera")
		cam.zoom = Vector2.ONE * 0.5
		print(ShipPhysics.integrateForces)
		ShipPhysics.integrateForces = self.integrateForces
		ShipPhysics.sleeping = false
		print(ShipPhysics.integrateForces)

func _ready() -> void:
	$Area2D.body_entered.connect(_handlePlayer.bind(true))
	$Area2D.body_exited.connect(_handlePlayer.bind(false))
