extends PathFollow2D


@export var duration: float = 1.0

var tween: Tween
var bucket: int = 0


func _handlePlayer(_body: Node2D, didEnter: bool) -> void:
	print("Lift player " + str(_body) + " entered? " + str(didEnter))
	if didEnter: bucket += 1
	else: bucket = max(0, bucket - 1)
	
	if tween != null: tween.kill()
	tween = get_tree().create_tween()
	
	if not didEnter and bucket == 0:
		var d: float = unit_offset * duration
		$RigidDynamicBody2D/AudioStreamPlayer2D/AnimationPlayer.play("MidLift")
		await tween.tween_property(self, "unit_offset", 0.0, d).finished
		$RigidDynamicBody2D/AudioStreamPlayer2D/AnimationPlayer.stop()
		$RigidDynamicBody2D/AudioStreamPlayer2D.playing = false
	elif didEnter and bucket == 1:
		var d: float = (1. - unit_offset) * duration
		$RigidDynamicBody2D/AudioStreamPlayer2D/AnimationPlayer.play("MidLift")
		await tween.tween_property(self, "unit_offset", 1.0, d).finished
		$RigidDynamicBody2D/AudioStreamPlayer2D/AnimationPlayer.stop()
		$RigidDynamicBody2D/AudioStreamPlayer2D.playing = false


func _ready() -> void:
	$Area2D.body_entered.connect(_handlePlayer.bind(true))
	$Area2D.body_exited.connect(_handlePlayer.bind(false))
