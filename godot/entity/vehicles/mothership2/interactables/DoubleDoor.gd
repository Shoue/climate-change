extends Node2D


@onready var TopFollow: PathFollow2D = $TopPath2D/TopFollow
@onready var BottomFollow: PathFollow2D = $BottomPath2D/BottomFollow

var tweenTop: Tween
var tweenBottom: Tween
var bucket: int = 0


func _handlePlayer(_body: Node2D, didEnter: bool) -> void:
	print("DoubleDoor player " + str(_body) + " entered? " + str(didEnter))
	if didEnter: bucket += 1
	else: bucket = max(0, bucket - 1)
	
	tweenTop = get_tree().create_tween()
	tweenBottom = get_tree().create_tween()
	
	if didEnter and bucket == 1:
		tweenTop.tween_property(TopFollow, "unit_offset", 1.0, 0.2)
		tweenBottom.tween_property(BottomFollow, "unit_offset", 1.0, 0.2)
	
	elif not didEnter and bucket == 0:
		tweenTop.tween_property(TopFollow, "unit_offset", 0.0, 0.4)
		tweenBottom.tween_property(BottomFollow, "unit_offset", 0.0, 0.4)


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	$Area2D.body_entered.connect(_handlePlayer.bind(true))
	$Area2D.body_exited.connect(_handlePlayer.bind(false))
