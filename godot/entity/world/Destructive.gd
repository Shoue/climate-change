extends Node2D

class_name Destructive

const DATA_NAME = "Destructive"

@export var tileMapPath: NodePath
@export var layer: int = -1
@export var healthySourceId: int = -1
@export var destroyedSourceId: int = -1

@onready var tileMap: TileMap = get_node(tileMapPath)
@onready var DA: Node2D = $DamageArea
@onready var AudioPlayer: AnimationPlayer = $AudioStreamPlayer2D/AnimationPlayer

var localTileMap: TileMap = null


func getTileMap() -> TileMap:
	return tileMap

func registerLocal(ltm: TileMap) -> void:
	localTileMap = ltm


@rpc(authority)
func _toggleDestroyed(c: Vector2i, isDestroyed: bool) -> void:
	if is_multiplayer_authority():
		_toggleDestroyed.rpc(c, isDestroyed)
	
	var sourceId = destroyedSourceId if isDestroyed else healthySourceId
	var aCoords: Vector2 = tileMap.get_cell_atlas_coords(layer, c, false)
	
	tileMap.set_cell(layer, c, sourceId, aCoords)
	if localTileMap != null: localTileMap.set_cell(layer, c, sourceId, aCoords)
	
	if isDestroyed:
		AudioPlayer.play("Clang1")


func _ready() -> void:
	print(tileMap.get_layer_name(layer))
	for c in tileMap.get_used_cells(layer):
		var atlasCoords: Vector2 = tileMap.get_cell_atlas_coords(layer, c, false)
		var sourceId: int = tileMap.get_cell_source_id(layer, c, false)
		var source: TileSetAtlasSource = tileMap.tile_set.get_source(sourceId)
		var isDestructive: bool = source.get_tile_data(atlasCoords, 0).get_custom_data(DATA_NAME)
		
		if not isDestructive: continue
		
		var pos: Vector2 = tileMap.map_to_world(c)
		var damageArea = DA.duplicate()
		
		tileMap.add_child(damageArea)
		
		damageArea.position = pos
		
		damageArea.destroyed.connect(_toggleDestroyed.bind(c, true))
		damageArea.healthy.connect(_toggleDestroyed.bind(c, false))
	
	DA.queue_free()
