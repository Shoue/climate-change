extends Area2D

class_name DamageArea

signal destroyed
signal healthy

@onready var HealthManager = $HealthManager

var tween: Tween = null


func _handleHealthChanged(oldHealth: float, newHealth: float) -> void:
	tween = get_tree().create_tween()
	tween.tween_property(HealthManager, "modulate", Color.WHITE, 0.2)
	tween.tween_property(HealthManager, "modulate", Color.TRANSPARENT, 0.5)
	
	if oldHealth >= 50.0 and newHealth < 50.0:
		destroyed.emit()
	
	elif oldHealth < 50.0 and newHealth >= 50.0:
		healthy.emit()


func _ready() -> void:
	HealthManager.healthChanged.connect(_handleHealthChanged)
	HealthManager.modulate = Color.TRANSPARENT
