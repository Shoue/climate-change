extends Node2D


@export var speed: Vector2 = Vector2.ONE * 256

@onready var Sprite: AnimatedSprite2D = $VisualPart/AnimatedSprite2D
@onready var AmbianceStream: AudioStreamPlayer2D = $VisualPart/AmbianceStream
@onready var Particles: GPUParticles2D = $VisualPart/GPUParticles2D
@onready var AttackArea: Area2D = $PhysicsPart/AttackArea
@onready var VisionArea: Area2D = $PhysicsPart/VisionArea
@onready var Physics: PhysicsPart = $PhysicsPart
@onready var Visuals: VisualPart = $VisualPart
@onready var HealthManager: HealthManager = $VisualPart/HealthManager
@onready var spawnPoint := Vector2.ZERO

var target: Node2D = null
var attackBodies: Array[Node2D] = []


func _handleBody(body: Node2D, didEnter: bool) -> void:
	print("DrillDrone handle body " + str(body) + " entered? " + str(didEnter))
	if didEnter and target == null:
		Sprite.play("Drill")
		Physics.sleeping = false
		target = body
	
	elif not didEnter and target == body:
		Sprite.play("Idle")
		target = null

func _handleAttack(body: Node2D, didEnter: bool) -> void:
	print("DrillDrone handling attack " + str(body) + " entered? " + str(didEnter))
	if didEnter:
		if not attackBodies.has(body): attackBodies.append(body)
	elif not didEnter:
		attackBodies.erase(body)
	
	Particles.emitting = attackBodies.size() > 0

func _handleDamage() -> void:
	for body in attackBodies:
		body = body.Root if body.get("Root") != null else body
		if body.get("HealthManager"):
			body.HealthManager.damage(1)


func integrateForces(state: PhysicsDirectBodyState2D) -> void:
	if not is_instance_valid(target): return
	
	var dir: Vector2 = (target.global_position - AttackArea.global_position).normalized()
	
	var targetVelocity = dir * speed
	state.linear_velocity = state.linear_velocity * 0.9 + targetVelocity * 0.1

func _handleDeath() -> void:
	queue_free()


func _ready() -> void:
	Physics.init(Visuals)
	Visuals.init(Physics)
	
	VisionArea.body_entered.connect(_handleBody.bind(true))
	VisionArea.body_exited.connect(_handleBody.bind(false))
	
	AttackArea.area_entered.connect(_handleAttack.bind(true))
	AttackArea.area_exited.connect(_handleAttack.bind(false))
	AttackArea.body_entered.connect(_handleAttack.bind(true))
	AttackArea.body_exited.connect(_handleAttack.bind(false))
	
	HealthManager.healthDepleted.connect(_handleDeath)
	
	$Timer.timeout.connect(_handleDamage)
	
	$VisualPart/AmbianceStream/AnimationPlayer.play("Loop")
	
	Physics.integrateForces = self.integrateForces
	
	call_deferred("deferredReady")

func deferredReady() -> void:
	spawnPoint = global_position
