extends RigidDynamicBody2D

const speed := Vector2(6, 8)

@export var networkPosition := Vector2.ZERO
@export var networkSpeed := Vector2.ZERO

@onready var HealthManager: HealthManager = $HealthManager
@onready var spawnPoint := Vector2.ZERO
var Physics = self
var Root = self
var Visuals = self

var tracking: Node2D = null
var shockwaveBodies: Array[Node2D] = []


@rpc(authority)
func setPosition(p: Vector2) -> void:
	Physics.global_position = p


func _removeTracking(body: Node2D) -> void:
	if is_instance_valid(body):
		body.tree_exiting.disconnect(_removeTracking)
		if body == tracking: tracking = null

func _setTracking(body: Node2D) -> void:
	if tracking == null:
		print("Doomball.setTracking = " + str(body))
		tracking = body
		tracking.tree_exiting.connect(_removeTracking.bind(body))

func _handleShockwave(body: Node2D, didEnter: bool) -> void:
	if didEnter and not shockwaveBodies.has(body): shockwaveBodies.append(body)
	elif not didEnter: shockwaveBodies.erase(body)

func _handleShockTick() -> void:
	for body in shockwaveBodies:
		body = body.Root if body.get("Root") != null else body
		
		if body.get("HealthManager"):
			body.HealthManager.damage(1)

func _handleDeath() -> void:
	global_position = spawnPoint
	HealthManager.setHealth(100)


func _ready() -> void:
	$VisibleArea.body_entered.connect(_setTracking)
	$VisibleArea.body_exited.connect(_removeTracking)
	
	$ShockwaveArea.area_entered.connect(_handleShockwave.bind(true))
	$ShockwaveArea.area_exited.connect(_handleShockwave.bind(false))
	$ShockwaveArea.body_entered.connect(_handleShockwave.bind(true))
	$ShockwaveArea.body_exited.connect(_handleShockwave.bind(false))
	
	$Timer.timeout.connect(_handleShockTick)
	
	$AudioStreamPlayer2D/AnimationPlayer.play("Loop")
	
	HealthManager.healthDepleted.connect(_handleDeath)
	
	call_deferred("deferredReady")

func deferredReady() -> void:
	spawnPoint = global_position

func _physics_process(delta: float) -> void:
	if is_multiplayer_authority():
		var time: float = Time.get_unix_time_from_system()
		var diff: Vector2 = Vector2.ZERO if tracking == null else tracking.global_position - global_position
		var move: Vector2 = diff.normalized() + Vector2.UP * cos(time * 2.0)
		
		networkSpeed = move * speed
		move_and_collide(networkSpeed)
		networkPosition = global_position
	
	else:
		if global_position.distance_to(networkPosition) > 8.0:
			global_position = networkPosition
		move_and_collide(networkSpeed)
