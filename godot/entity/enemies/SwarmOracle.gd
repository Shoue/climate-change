extends Node2D


signal dangerDepleted


class SwarmEnemy:
	var scene: PackedScene
	
	func _init(s: PackedScene):
		scene = s

class AssignedSwarmEnemy:
	var node: Node2D
	var index: int = -1
	
	func _init(n: Node2D, ix: int):
		node = n
		index = ix

class Swarm:
	var index: int
	var enemies: Array[AssignedSwarmEnemy] = []
	
	func _init(ix: int, es: Array[AssignedSwarmEnemy]):
		index = ix
		enemies = es
	
	func getEnemies() -> Array[AssignedSwarmEnemy]:
		return enemies

const EnemyType := {
	"DrillDrone": "drill_drone",
	"Doomball": "doomball",
}

@onready var T: Timer = $Timer

var enemies := {
	"drill_drone": SwarmEnemy.new(
		preload("res://entity/enemies/drill_drone/DrillDrone.tscn")
	),
	"doomball": SwarmEnemy.new(
		preload("res://entity/enemies/doomball/Doomball.tscn")
	),
}
var swarmIx: int = -1
# swarms : { [K: int]: Array<SwarmEnemy> }aa
var swarms: Dictionary = {}
var dangerScore: int = 0


func createSwarm(enemyName: String, size: int) -> Swarm:
	if not (enemyName in enemies) or size <= 0: return null
	
	swarmIx += 1
	var ix = swarmIx
	var enemy: SwarmEnemy = enemies[enemyName]
	var assignedEnemies: Array[AssignedSwarmEnemy] = []
	
	for _i in range(0, size):
		var node: Node2D = enemy.scene.instantiate()
		node.tree_exiting.connect(_handleEnemyDead.bind(node))
		var assignedEnemy := AssignedSwarmEnemy.new(node, ix)
		assignedEnemies.append(assignedEnemy)
	
	return Swarm.new(ix, assignedEnemies)

func changeDangerScore(i: int) -> void:
	var nds: int = max(0, dangerScore + i)
	
	if dangerScore != 0 and nds == 0:
		T.start(30)

func start() -> void:
	T.start(30)


func _handleEnemyDead(_body: Node2D) -> void:
	changeDangerScore(-1)

func _handleDangerTimeout() -> void:
	self.dangerDepleted.emit()


func _ready() -> void:
	T.timeout.connect(_handleDangerTimeout)
