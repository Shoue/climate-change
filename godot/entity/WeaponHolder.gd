extends Node2D

class_name WeaponHolder


var weapon: Weapon = null
var slots: Dictionary = {}


func setSlot(ix: int, w: Weapon) -> void:
	slots[ix] = w

func activateSlot(ix: int) -> void:
	if weapon != null: weapon.visible = false
	
	weapon = slots.get(ix)
	
	if weapon != null: weapon.visible = true

func shoot(dir: Vector2) -> void:
	if not weapon: return
	
	var rotDir: Vector2 = dir.normalized().rotated(-rotation)
	print("WH.shoot, dir = " + str(dir) + ", rotation = " + str(rad2deg(rotation)) + ", rotDir = " + str(rotDir))
	weapon.shoot(dir)

func deactivate() -> void:
	if not weapon: return
	weapon.deactivate()

func setRotation(a: float) -> void:
	rotation = a


func _ready() -> void:
	setSlot(1, $RocketLauncher)
	setSlot(2, $RepairTool)
	
	for c in get_children():
		c.visible = false
	
	activateSlot(1)
