extends Node2D

class_name HealthManager

signal healthChanged (oldHealth, health)
signal healthDepleted


@onready var HealthBar: ProgressBar = $ProgressBar


var health: float = 100.0


func damage(a: float) -> void:
	setHealth(health - a)

func heal(a: float) -> void:
	setHealth(health + a)

func setHealth(h: float) -> void:
	if h != health:
		var newHealth = clamp(h, 0.0, 100.0)
		HealthBar.value = newHealth
		self.healthChanged.emit(health, newHealth)
		health = newHealth
		if health == 0.0: self.healthDepleted.emit()

func getHealth() -> float:
	return health


func _ready() -> void:
	HealthBar.value = health
