extends Node2D


@onready var A: Area2D = $Area2D
@onready var HM: HealthManager = $HealthManager
@onready var T: Timer = $Timer
@onready var AP: AnimationPlayer = $AudioStreamPlayer2D/AnimationPlayer

var healingBodies: Array[Node2D] = []


func _handleBody(body: Node2D, didEnter: bool) -> void:
	body = body.Root if body.get("Root") != null else body
	if body.get("HealthManager") == null: return
	
	if didEnter:
		healingBodies.append(body)
		if T.is_stopped(): T.start()
		
		if body.HealthManager.getHealth() == 100.0:
			pass
		elif HM.getHealth() < 10.0:
			AP.play("DepletedBeep")
		else:
			AP.play("ActiveBeep")
	
	else:
		healingBodies.erase(body)

func _handleTick() -> void:
	for b in healingBodies:
		var body: Node2D = b.Root if b.get("Root") != null else b
		
		if HM.getHealth() < 10.0:
			break
		
		if body.get("HealthManager") and body.HealthManager.getHealth() < 100.0:
			body.HealthManager.heal(10)
			HM.damage(10)
	
	HM.heal(3)


func _ready() -> void:
	A.body_entered.connect(_handleBody.bind(true))
	A.body_exited.connect(_handleBody.bind(false))
	
	T.timeout.connect(_handleTick)
