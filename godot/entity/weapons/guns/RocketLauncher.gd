extends Weapon


const Rocket: PackedScene = preload("res://entity/weapons/guns/Rocket.tscn")

@onready var AudioPlayer: AnimationPlayer = $AudioStreamPlayer2D/AnimationPlayer
@onready var T: Timer = $Timer


func shoot(dir: Vector2) -> void:
	if T.is_stopped():
		dir = dir.normalized()
		var RocketBullet = Rocket.instantiate()
		RocketBullet.global_position = global_position + dir * 32.0
		RocketBullet.direction = dir
		RocketBullet.rotation = dir.angle()
		RocketBullet.speed = Vector2(512, 512)
		get_node("/root/World/EntityLayer").add_child(RocketBullet)
		AudioPlayer.stop(true)
		AudioPlayer.play("Shoot")
		T.start(1)

func deactivate() -> void:
	pass
