extends Sprite2D

class_name Rocket


@export var direction := Vector2.ZERO
@export var speed := Vector2.ZERO

@onready var A: Area2D = $Area2D
@onready var P: GPUParticles2D = $GPUParticles2D
@onready var S: AudioStreamPlayer2D = $AudioStreamPlayer2D
@onready var T: Timer = $Timer

var done := false
var bodies: Array[Node2D]


func explode() -> void:
	if done: return
	
	done = true
	
	for body in bodies:
		body.HealthManager.damage(20)
	
	direction = Vector2.ZERO
	P.emitting = false
	visible = false
	S.playing = true
	await S.finished
	
	queue_free()

func _hitBody(body: Node2D) -> void:
	body = body.Root if body.get("Root") != null else body
	if body.get("HealthManager") != null:
		bodies.append(body)
	
	explode()

func _registerBody(body: Node2D, didEnter: bool) -> void:
	body = body.Root if body.get("Root") != null else body
	if body.get("HealthManager") == null: return
	
	if didEnter:
		bodies.append(body)
	else:
		bodies.erase(body)


func _ready() -> void:
	A.body_entered.connect(_hitBody)
	A.area_entered.connect(_registerBody.bind(true))
	A.area_exited.connect(_registerBody.bind(false))
	T.timeout.connect(explode)

func _physics_process(delta: float) -> void:
	global_position += direction * delta * speed
