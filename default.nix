{ ... }:

let
  sources = import ./nix/sources.nix {};
  pkgs = import sources.nixpkgs {};
  stdenv = pkgs.stdenv;
  lib = pkgs.lib;

  godot4alpha12-bin = stdenv.mkDerivation rec {
    pname = "godot";
    version = "4.0-alpha12";

    src = pkgs.fetchzip {
      url = "https://downloads.tuxfamily.org/godotengine/4.0/alpha12/Godot_v4.0-alpha12_linux.64.zip";
      sha256 = "sha256-0ejF56GYyTg7sTJeZ5Bqf/6Y8sJy2H42+V8jh8D1u2E=";
    };

    nativeBuildInputs = (with pkgs; [
      autoPatchelfHook makeWrapper
    ]);

    buildInputs = (with pkgs; with pkgs.xlibs; [
      udev
      libX11
      libXcursor
      libXinerama
      libXrandr
      libXrender
      libXi
      libXext
      libXfixes
      freetype
      openssl
      alsaLib
      libpulseaudio
      libGLU
      vulkan-loader
      zlib
      yasm
    ]);

    sourceRoot = ".";

    installPhase = ''
      install -m755 -D $src/Godot_v4.0-alpha12_linux.64 $out/bin/godot

      wrapProgram $out/bin/godot \
        --prefix LD_LIBRARY_PATH : ${lib.makeLibraryPath buildInputs}
    '';

    meta = (with lib; {
      homepage = "https://godotengine.org/";
      description = "Godot";
      platforms = platforms.linux;
    });
  };

in

stdenv.mkDerivation rec {
  pname = "climate-change";
  version = "0.1";

  buildInputs = (with pkgs; [
    godot4alpha12-bin
    niv
  ]);
}
