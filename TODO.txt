Codename: Climate Change
========================

CORE IDEAS
- Large maps
  - Open world?
- Verticality
  - We don't want a ship that goes through tunnels (kind of like Barotrauma), we want
    an open-ish world with floating islands to explore
  - Should we even _have_ flooring at the bottom? What if it's all floating islands?
    - I guess it can also depend on the map or area, just avoid big flat plains and we're good
- Exploration balanced with fighting
  - Barotrauma punishes you for straying too far, as creatures attack the ship
    but we want to encourage exploration and leave the ship mostly to its own devices
    until large scale attacks happen (and you're warned they will).
  - Finding old ships to loot
  - Finding resources to extract
    - Oil? (to power the ships?)
    - Spice? (to sell only?)
    - Metals? (to repair the ship? build weapons?)
  - Finding powerful enemies that give rare materials
    - Rocket ammo
    - Laser gun ammo
    - Rare metals
- Construction
  - The ship can break
    - Walls
    - Floor
    - Doors
    - Lifts?
  - Repairing
    - We need a repair tool!
- Flying
  - Both the mothership and the dropships should be flyable
